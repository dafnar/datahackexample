from bson.code import Code
import pymongo

CONNECTION_URI = 'mongodb://localhost'

# Connect to DB
db_client = pymongo.MongoClient(CONNECTION_URI)
db_datahack = db_client.datahack

# Get collections
groups = db_datahack.groups
participants = db_datahack.participants

def get_academic_fields():
    """Returns a list of academic fields and their count.

    Note that here we are using a simple way to count all distinct values,
    it is not the most efficient way - but simple and readable.
    """
    academic_fields = []
    for field in participants.distinct('academic_field'):
        count = participants.find({'academic_field': field}).count()
        academic_fields.append({
            'name': field,
            'count': count,
        })
    return academic_fields

def get_academic_status():
    """Returns a list of academic status values and their count.

    Note that this is similar to get_academic_fields - but here we use
    group and reduce.
    """
    return participants.group(
        key={'academic_status': 1},
        condition={},
        initial={'count': 0},
        reduce=Code('function(obj, prev) { prev.count++; }'),
        finalize=Code('function(result) { result.name = result.academic_status; }'),
    )

def get_universities():
    """Returns a list of the participant universities and their count.

    Note that in this example we are passing a condition - we don't want
    to count empty universities.
    """
    return participants.group(
        key={'university': 1},
        condition={'university': {'$ne': None}},
        initial={'count': 0},
        reduce=Code('function(obj, prev) { prev.count++; }'),
        finalize=Code('function(result) { result.name = result.university; }'),
    )

def get_groups():
    """Returns a dict between group sizes and a list of length 2:
    [total_groups_count, groups_with_an_idea]
    """
    groups_stats = {}
    for group_size in groups.distinct('size'):
        total_count = groups.find({'size': group_size}).count()
        has_idea_count = groups.find(
            {'size': group_size, 'has_idea': True}
        ).count()
        groups_stats[group_size] = [total_count, has_idea_count]
    return groups_stats


