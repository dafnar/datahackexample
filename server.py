import os
import json
import tornado.ioloop
import tornado.web
import db_handler

PATH = os.path.abspath(os.path.dirname(__file__))
RESULTS_THRESHOLD = 5

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("%s/templates/index.html" % PATH)

class GroupsHandler(tornado.web.RequestHandler):
    def get(self):
        groups = db_handler.get_groups()
        self.render(
            "%s/templates/status.html" % PATH,
            groups=json.dumps(groups),
        )

class AcademicHandler(tornado.web.RequestHandler):
    def get(self):
        academic_fields = self._compress_results(
            db_handler.get_academic_fields(),
            RESULTS_THRESHOLD,
        )
        academic_status = self._compress_results(
            db_handler.get_academic_status(),
            RESULTS_THRESHOLD,
        )
        universities = self._compress_results(
            db_handler.get_universities(),
            RESULTS_THRESHOLD,
        )

        self.render(
            "%s/templates/fields.html" % PATH,
            acad=json.dumps(academic_fields),
            sta=json.dumps(academic_status),
            uni=json.dumps(universities),
        )

    def _compress_results(self, results, threshold):
        compressed = []
        others_count = 0
        for entry in results:
            if entry['count'] >= threshold:
                compressed.append(entry)
            else:
                others_count += entry['count']

        if others_count:
            compressed.append({'name': 'Others', 'count': others_count})
        return compressed


def make_app():
    return tornado.web.Application(
        [
            (r"/", MainHandler),
            (r"/groups", GroupsHandler),
            (r"/academy", AcademicHandler),
        ],
        debug=True,
        static_path=os.path.join(PATH, 'static')
    )

if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
